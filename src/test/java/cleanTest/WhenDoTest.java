package cleanTest;

import activities.whenDo.CreateTaskScreen;
import activities.whenDo.DeleteTaskScreen;
import activities.whenDo.MainScreen;
import activities.whenDo.UpdateTaskScreen;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Keys;
import session.Session;

import java.net.MalformedURLException;
import java.time.Duration;

public class WhenDoTest {

    private MainScreen mainScreen= new MainScreen();
    private CreateTaskScreen createTaskScreen= new CreateTaskScreen();
    private UpdateTaskScreen updateTaskScreen = new UpdateTaskScreen();
    private DeleteTaskScreen deleteTaskScreen = new DeleteTaskScreen();

    @Test
    public void verifyCreateTask() throws MalformedURLException {
        String title="Remove";

        mainScreen.addTaskButton.click();
        createTaskScreen.titleTextBox.setValue(title);
        createTaskScreen.descriptionTextBox.setValue("Remove this task");
        createTaskScreen.saveButton.click();

        Assert.assertEquals("ERROR! task was not created",title,mainScreen.nameTaskLabel.getText());
    }

   /* @After
    public void close() throws MalformedURLException {
        Session.getInstance().closeSession();
    }*/

    @After
    public void verifyUpdateTask() throws MalformedURLException {
        String title="Remove";

      updateTaskScreen.itemTextBox.click();
      updateTaskScreen.ItemActualizar.setValue("Campo Actualizado");
      updateTaskScreen.updateButton.click();

        //mainScreen.search.getControl().submit();
    //Assert.assertEquals("ERROR! task was not created",title,mainScreen.nameTaskLabel.getText());

    }

    @After
    public void verifyDeleteTask() throws MalformedURLException {
        String title="Remove";

        updateTaskScreen.itemTextBox.click();
        deleteTaskScreen.delete.click();
        deleteTaskScreen.confirmarDeleteButton.click();

        //mainScreen.search.getControl().submit();
        //Assert.assertEquals("ERROR! task was not created",title,mainScreen.nameTaskLabel.getText());

    }
}
