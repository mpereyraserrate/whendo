package basicTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BasicJunit {

    // setup - initialize
    @Before
    public void setup(){
        System.out.println("before");

    }
    // cleanup - teardown
    @After
    public void cleanup(){
        System.out.println("after");
    }

    @Test
    public void thisIsATest(){
        System.out.println("TESTTT");
    }

    @Test
    public void thisIsATest2(){
        System.out.println("TESTTT2");
    }

}
