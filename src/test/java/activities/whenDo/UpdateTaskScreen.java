package activities.whenDo;

import controlAppium.Button;
import controlAppium.TextBox;
import org.openqa.selenium.By;

public class UpdateTaskScreen {
    public TextBox itemTextBox = new TextBox(By.id("com.vrproductiveapps.whendo:id/home_list_item_text"));
    public TextBox ItemActualizar = new TextBox(By.id("com.vrproductiveapps.whendo:id/noteTextNotes"));
    public Button updateButton= new Button(By.id("com.vrproductiveapps.whendo:id/saveItem"));


    public UpdateTaskScreen(){}
}
