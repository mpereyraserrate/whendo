package activities.whenDo;

import controlAppium.Button;
import controlAppium.TextBox;
import org.openqa.selenium.By;

public class DeleteTaskScreen {

    public TextBox itemTextBox = new TextBox(By.id("com.vrproductiveapps.whendo:id/home_list_item_text"));
    public Button delete = new Button(By.id("com.vrproductiveapps.whendo:id/deleteItem"));
    public Button confirmarDeleteButton= new Button(By.id("android:id/button1"));

    public DeleteTaskScreen(){}
}
