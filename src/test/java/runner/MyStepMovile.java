package runner;

import activities.whenDo.CreateTaskScreen;
import activities.whenDo.MainScreen;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.net.MalformedURLException;
import java.util.Map;

public class MyStepMovile {
    private MainScreen mainScreen= new MainScreen();
    private CreateTaskScreen createTaskScreen= new CreateTaskScreen();

    @Given("la app Whendo esta abierta")
    public void laAppWhendoEstaAbierta() {
    }

    @And("yo hago click en el boton agregarTarea")
    public void yoHagoClickEnElBotonAgregarTarea() throws MalformedURLException {
        mainScreen.addTaskButton.click();
    }

    @When("yo creo una tarea")
    public void yoCreoUnaTarea(Map<String,String> datos) throws MalformedURLException {
        createTaskScreen.titleTextBox.setValue(datos.get("titulo"));
        createTaskScreen.descriptionTextBox.setValue(datos.get("descripcion"));
    }

    @And("realizo click en boton guardar")
    public void realizoClickEnBotonGuardar() throws MalformedURLException {
        createTaskScreen.saveButton.click();
    }

    @Then("la tarea con el nombre {string} deberia ser creada")
    public void laTareaConElNombreDeberiaSerCreada(String expectedTitle) throws MalformedURLException {
        Assert.assertEquals("ERROR! task was not created",expectedTitle,mainScreen.nameTaskLabel.getText());
    }
}